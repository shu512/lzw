#ifndef COMPRESS_H
#define COMPRESS_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int compress(char *src, char *dst, char *count);
int decompress(char *src, char *dst, char *count);


#endif