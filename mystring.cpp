#include "mystring.h"
#include <cstring>

int my_strlen(const char *str)
{
	int i = 0;
	while (str[i])
		i++;
	return i;
}

void my_strcpy(char *str1, const char *str2)
{
	int i, k;
	k = my_strlen(str2); //
	for (i = 0; i < k; i++)
		str1[i] = str2[i];
	str1[k] = '\0';
	//while (*str1++ = *str2++);
}

void my_strcat(char *str1, const char *str2)
{
	int i, k1, k2;
	k1 = my_strlen(str1);
	k2 = my_strlen(str2);
	for (i = 0; i < k2; i++)
		str1[i + k1] = str2[i];
	str1[k1 + k2] = '\0';
}

int my_strcmp(const char *str1, const char *str2)
{
	int i, k;
	k = my_strlen(str1);
	for (i = 0; i < k; i++)
		if (str1[i] != str2[i])
			return (str1[i] - str2[i]);
	if (str2[i] == '\0')
		return 0;
	else
		return -str2[i];
}