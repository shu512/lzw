#ifndef MYSTRING_H
#define MYSTRING_H

void my_strcpy(char *destptr, const char *srcptr);
int my_strlen(const char *str);
void my_strcat(char *str1, const char *str2);
int my_strcmp(const char *str1, const char *str2);

#endif