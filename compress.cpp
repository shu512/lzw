#include "compress.h"
#include "mystring.h"

int load_dict(char **dict, int SIZE_DICT)
{
	int i;
	for (i = 0; i < SIZE_DICT; i++)
		dict[i] = (char*)malloc(10 * sizeof(char));
	for (i = 0; i < 256; i++)
	{
		dict[i][0] = char(i);
		dict[i][1] = '\0';
	}
	return i;
}

void free_dict(char **dict, int SIZE_DICT)
{
	int i;
	for (i = 0; i < SIZE_DICT; i++)
		free(dict[i]);
	free(dict);
}

int MayBeNotInt(char *count)
{
	int i, k;
	k = my_strlen(count);
	for (i = 0; i < k; i++)
	{
		if ((count[i] > 57) || (count[i] < 48))
			return 1;
	}
	return -1;
}

int decompress(char *src, char *dst, char *count)
{
	int i, j, k, sizeDict = 0, w, SIZE_DICT;
	char p[10] = "\0", c[10] = "\0", pc[10] = "\0", temp[10];
	char ch;
	char *coding = (char*)malloc(1000 * sizeof(char));
	coding[0] = '\0';
	if (MayBeNotInt(count) == -1)
		SIZE_DICT = atoi(count);
	else
		return 5;
	char **dict = (char**)malloc(sizeof(char *) * SIZE_DICT);
	sizeDict = load_dict(dict, SIZE_DICT);

	int pW, cW;
	char strcW[10] = "\0", strpW[10] = "\0";
	int flag = 0;
	FILE *in = fopen(src, "rb");
	FILE *out = fopen(dst, "w");
	if (!in)
	{
		return 4;
	}
	i = 0;
	while (!feof(in)) {
		ch = fgetc(in);
		if (ch != ' ') {
			temp[i] = ch;
			i++;
		}
		else
		{
			temp[i] = '\0';
			cW = atoi(temp);
			my_strcpy(strcW, dict[cW]);
			my_strcpy(pc, strpW);
			my_strcat(pc, strcW);
			w = 0;
			for (j = 0; j < sizeDict; j++)
				if (my_strcmp(dict[j], pc) == 0)
				{
					w = 1;
					break;
				}
			if (w == 0)
			{
				if (sizeDict == SIZE_DICT)
					sizeDict = 256;
				char temp1[10];
				my_strcpy(temp1, pc);
				for (j = 1; j <= my_strlen(temp1); j++)
				{
					my_strcpy(pc, temp1);
					pc[j] = '\0';

					w = 0;
					for (k = 0; k < sizeDict; k++)
						if (my_strcmp(dict[k], pc) == 0)
						{
							w = 1;
							break;
						}
					if (w == 0)
					{
						my_strcpy(dict[sizeDict], pc);
						sizeDict++;
						pc[j - 1] = '\0';
						my_strcat(coding, strpW);
						break;
					}
				}
			}
			pW = cW;
			my_strcpy(strpW, dict[pW]);
			temp[0] = '\0';
			i = 0;
		}
	}
	my_strcat(coding, strpW);
//	puts(coding);
	fwrite(coding, 1, my_strlen(coding), out);
	free_dict(dict, SIZE_DICT);

	return 0;
}

int compress(char *src, char *dst, char *count)
{
	int i, j, k, sizeDict = 0, w, SIZE_DICT;
	char p[10] = "\0", c[10] = "\0", pc[10] = "\0", temp[10];
	char ch;
	char *str = (char*)malloc(1000 * sizeof(char));
	char *coding = (char*)malloc(1000 * sizeof(char));
	coding[0] = '\0';
	if (MayBeNotInt(count) == -1)
		SIZE_DICT = atoi(count);
	else
		return 5;
	char **dict = (char**)malloc(sizeof(char *) * SIZE_DICT);
	sizeDict = load_dict(dict, SIZE_DICT);

	FILE *in = fopen(src, "r");
	FILE *out = fopen(dst, "wb");
	if (!in)
	{
		return 4;
	}
	i = 0;
	while (!feof(in)) {
		ch = fgetc(in);
		str[i++] = ch;
	}
	str[i - 1] = '\0';
	for (i = 0; i < my_strlen(str); i++)
	{
		c[0] = str[i];
		c[1] = '\0';
		my_strcpy(pc, p);
		my_strcat(pc, c);
		w = 0;
		for (j = 0; j < sizeDict; j++)
		{
			if (my_strcmp(pc, dict[j]) == 0)
			{
				w = 1;
				my_strcpy(p, pc);
				break;
			}
		}
		if (w == 0)
		{
			if (sizeDict == SIZE_DICT)
				sizeDict = 256;
			char temp1[10];
			my_strcpy(temp1, pc);
			for (j = 1; j <= my_strlen(temp1); j++)
			{
				my_strcpy(pc, temp1);
				pc[j] = '\0';

				w = 0;
				for (k = 0; k < sizeDict; k++)
					if (my_strcmp(dict[k], pc) == 0)
					{
						w = 1;
						break;
					}
				if (w == 0)
				{
					my_strcpy(dict[sizeDict], pc);
					sizeDict++;
					pc[j - 1] = '\0';
					my_strcpy(p, pc);
					break;
				}
			}
			for (j = 0; j < sizeDict; j++)
				if (my_strcmp(p, dict[j]) == 0)
				{
					itoa(j, temp, 10);
					my_strcat(coding, temp);
					temp[0] = ' ';
					temp[1] = '\0';
					my_strcat(coding, temp);
					break;
				}
			my_strcpy(p, c);
			p[my_strlen(c)] = '\0';
		}
	}
	for (j = 0; j < sizeDict; j++)
		if (my_strcmp(c, dict[j]) == 0)
		{
			itoa(j, temp, 10);
			my_strcat(coding, temp);
			temp[0] = '\0';
			break;
		}
	my_strcat(coding, " ");
//	puts(coding);
	fwrite(coding, 1, my_strlen(coding), out);
	free_dict(dict, SIZE_DICT);
	return 0;
}