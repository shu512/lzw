#include "compress.h"
#include "mystring.h"

int main(int argc, char *argv[])
{
	int er;
	if (argc == 6)
	{
		if (strcmp(argv[2], "-o") == 0)
		{
			if (strcmp(argv[1], "-c") == 0)
			{
				er = compress(argv[4], argv[3], argv[5]);
				switch (er) {
				case 4:
					printf("Error 'in' file!\n");
					return -1;
				case 5:
					printf("Error fifth argument!\n");
					return -1;
				}
			
			}
			else
			{
				if (strcmp(argv[1], "-d") == 0)
				{
					er = decompress(argv[4], argv[3], argv[5]);
					if (er == 5) //case?
					{
						printf("Error fifth argument!\n");
						return -1;
					}
				}
				else
				{
					printf("Error first argument!\n");
					return -1;
				}
			}
		}
		else
		{
			printf("Error second argument!\n");
			return -1;
		}
	}
	else
	{
		printf("Please, enter 5 arguments!\n");
		return -1;
	}
	return 0;
}